import {StyleSheet} from 'react-native';

const globalStyles = StyleSheet.create({
  title: {
    fontFamily: 'Lato',
    textAlign: 'center',
    color: '#34495e',
    fontSize: 32,
  },
  inputField: {
    backgroundColor: '#fff',
    marginVertical: 10,
    marginHorizontal: 20,
    padding: 10,
    fontSize: 22,
    fontWeight: '700',
    width: '90%',
  },
  label: {
    marginHorizontal: 20,
  },
  inputAlert: {
    marginHorizontal: 20,
    color: '#e74c3c',
  },
});

export default globalStyles;
