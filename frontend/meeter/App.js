import 'react-native-gesture-handler';
import React from 'react';
import {View, Text, Button} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, StackNavigator} from '@react-navigation/stack';

import InitMeeting from './screens/InitMeeting';
import Home from './screens/Home';
import Meeting from './screens/Meeting';
import MyMeetings from './screens/MyMeetings';
import Map from './screens/Map';
import Suggestions from './screens/Suggestions';
import Join from './screens/Join';
import SignUp from './screens/SignUp';
import SignIn from './screens/SignIn';
import Settings from './screens/Settings';
import Voting from './screens/Voting';
import Decision from './screens/Decision';
const Stack = createStackNavigator();

const App: () => React$Node = () => {
  return (
    /* TODO: Wypadałoby przeniesc ten navigator do osobnego pliku / plików np w routes/ */
    <NavigationContainer>
      <Stack.Navigator initialRouteName="SignIn">
        <Stack.Screen
          name="Home"
          component={Home}
          options={{title: 'Meeter', headerLeft: null}}
        />
        <Stack.Screen
          name="InitMeeting"
          component={InitMeeting}
          options={{title: 'Zorganizuj spotkanie'}}
        />
        <Stack.Screen
          name="Voting"
          component={Voting}
          options={{title: 'Głosuj'}}
        />
        <Stack.Screen
          name="Decision"
          component={Decision}
          options={{title: 'Wybierz miejsce'}}
        />
        <Stack.Screen
          name="Meeting"
          component={Meeting}
          options={{title: 'Spotkanie'}}
        />
        <Stack.Screen
          name="MyMeetings"
          component={MyMeetings}
          options={{title: 'Moje spotkania'}}
        />
        <Stack.Screen name="Map" component={Map} options={{title: 'Mapa'}} />
        <Stack.Screen
          name="Suggestions"
          component={Suggestions}
          options={{title: 'Propozycje'}}
        />
        <Stack.Screen
          name="Join"
          component={Join}
          options={{title: 'Dołącz do spotkania'}}
        />
        <Stack.Screen
          name="SignUp"
          component={SignUp}
          options={{title: 'Zarejestruje się'}}
        />
        <Stack.Screen
          name="SignIn"
          component={SignIn}
          options={{title: 'Zaloguj się'}}
        />
        <Stack.Screen
          name="Settings"
          component={Settings}
          options={{title: 'Ustawienia'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
