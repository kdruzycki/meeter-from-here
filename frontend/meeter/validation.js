const validName = name => {
  let minLen = 5;
  let maxLen = 32;

  if (name.length < minLen)
    return {valid: false, alert: `Nazwa musi mieć minimum ${minLen} znaków`};

  if (name.length > maxLen)
    return {valid: false, alert: `Nazwa musi mieć maksimum ${maxLen} znaków`};

  return {valid: true};
};

const validDate = date => {
  let currDate = new Date();

  if (date < currDate) return {valid: false, alert: `Podaj przyszłą datę`};

  return {valid: true};
};

export {validDate, validName};
