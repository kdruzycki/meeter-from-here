import axios from 'axios';
const apiUrl = 'https://zawirowania.pl/backend/event.php';

const apiPost = async bodyForm => {
  return axios({
    method: 'post',
    url: apiUrl,
    data: bodyForm,
  })
    .then(response => response.data)
    .catch(error => ({errorMessage: error}));
};

const genSignInForm = (username, password) => {
  let bodyForm = new FormData();
  bodyForm.append('eventName', 'UserLoggedIn');
  bodyForm.append('sessionID', '');
  bodyForm.append(
    'eventData',
    `{	
      "username_or_email":"${username}",	
      "password":"${password}" 
    }`,
  );

  return bodyForm;
};

const genSignUpForm = (username, email, password) => {
  let bodyForm = new FormData();
  bodyForm.append('eventName', 'UserCreated');
  bodyForm.append('sessionID', '');
  bodyForm.append(
    'eventData',
    `{	
      "username":"${username}",	
      "email":"${email}",	
      "password":"${password}" 
    }`,
  );

  return bodyForm;
};

const genSignOutForm = sessionId => {
  let bodyForm = new FormData();
  bodyForm.append('eventName', 'UserLoggedOut');
  bodyForm.append('sessionID', sessionId);

  return bodyForm;
};

const genUserJoinedForm = (sessionId, meetingID, location) => {
  let bodyForm = new FormData();
  bodyForm.append('eventName', 'UserJoined');
  bodyForm.append('sessionID', sessionId);
  bodyForm.append(
    'eventData',
    `{	
    "meetingID": "${meetingID}",
    "userLocation": {
      "latitude":"${location['latitude']}",
      "longitude":"${location['longitude']}"
    }
  }`,
  );

  return bodyForm;
};

const genGeneratePlacesForm = (sessionId, meetingID) => {
  let bodyForm = new FormData();
  bodyForm.append('eventName', 'GeneratePlaces');
  bodyForm.append('sessionID', sessionId);
  bodyForm.append(
    'eventData',
    `{	
    "meetingID": "${meetingID}"
  }`,
  );

  return bodyForm;
};

const genVoteForPlacesForm = (sessionId, meetingId, placeIDs) => {
  let bodyForm = new FormData();
  bodyForm.append('eventName', 'VoteForPlaces');
  bodyForm.append('sessionID', sessionId);
  bodyForm.append(
    'eventData',
    `{	
    "meetingID": "${meetingId}",
    "placeIDs":  [${placeIDs}] 
  }`,
  );

  return bodyForm;
};

const genUserStatusSetForm = (sessionId, meetingID, willAttend) => {
  let bodyForm = new FormData();
  bodyForm.append('eventName', 'UserStatusSet');
  bodyForm.append('sessionID', sessionId);
  bodyForm.append(
    'eventData',
    `{	
    "meetingID": "${meetingID}"
    "willAttend": "${willAttend}"
  }`,
  );

  return bodyForm;
};

const genSetDecisionForm = (sessionId, meetingID, statusChosen, placeID) => {
  let bodyForm = new FormData();
  bodyForm.append('eventName', 'SetDecision');
  bodyForm.append('sessionID', sessionId);
  bodyForm.append(
    'eventData',
    `{	
    "meetingID": "${meetingID}",
    "status": "${statusChosen}",
    "placeID": ${placeID}
  }`,
  );

  return bodyForm;
};

const genDownloadMembersForm = (sessionId, meetingID) => {
  let bodyForm = new FormData();
  bodyForm.append('eventName', 'DownloadMembers');
  bodyForm.append('sessionID', sessionId);
  bodyForm.append(
    'eventData',
    `{	
    "meetingID": "${meetingID}"
  }`,
  );

  return bodyForm;
};
const genDownloadPlacesForm = (sessionId, meetingId) => {
  let bodyForm = new FormData();
  bodyForm.append('eventName', 'DownloadPlaces');
  bodyForm.append('sessionID', sessionId);
  bodyForm.append(
    'eventData',
    `{	
    "meetingID": "${meetingId}"
  }`,
  );

  return bodyForm;
};

const genDownloadMeetingsForm = sessionId => {
  let bodyForm = new FormData();
  bodyForm.append('eventName', 'DownloadMeetings');
  bodyForm.append('sessionID', sessionId);

  return bodyForm;
};

const genChangeUsernameForm = (username, sessionId) => {
  let bodyForm = new FormData();
  bodyForm.append('eventName', 'UsernameAmended');
  bodyForm.append('sessionID', sessionId);
  bodyForm.append(
    'eventData',
    `{	
      "new_username": "${username}"
    }`,
  );

  return bodyForm;
};

const genUserStatusForm = (meetingId, sessionId, status) => {
  let bodyForm = new FormData();
  bodyForm.append('eventName', 'UserStatusSet');
  bodyForm.append('sessionID', sessionId);
  bodyForm.append(
    'eventData',
    `{	
      "meetingID": "${meetingId}",
      "willAttend": ${status}
    }`,
  );

  return bodyForm;
};

const genCreateMeetingForm = (name, time, location, sessionId) => {
  let bodyForm = new FormData();
  bodyForm.append('eventName', 'MeetingAdded');
  bodyForm.append('sessionID', sessionId);
  bodyForm.append(
    'eventData',
    `{	
      "name": "${name}",
      "time": "${time}",
      "userLocation": {
        "latitude": "${location['latitude']}",
        "longitude": "${location['longitude']}"
      }
    }`,
  );

  return bodyForm;
};

export {
  apiPost,
  genSignInForm,
  genSignUpForm,
  genSignOutForm,
  genUserJoinedForm,
  genGeneratePlacesForm,
  genVoteForPlacesForm,
  genUserStatusSetForm,
  genSetDecisionForm,
  genDownloadPlacesForm,
  genDownloadMembersForm,
  genDownloadMeetingsForm,
  genUserStatusForm,
  genChangeUsernameForm,
  genCreateMeetingForm,
};
