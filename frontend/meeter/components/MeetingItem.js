import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

const MeetingItem = ({
  title,
  sessionId,
  meetingId,
  navigation,
  username,
  willAttend,
  isOwner,
  statusOfMeeting,
  finalPlaceName,
  styleSel,
  time,
}) => {
  const viewMeeting = () => {
    navigation.navigate('Meeting', {
      sessionId: sessionId,
      meetingId: meetingId,
      username: username,
      willAttend: willAttend,
      title: title,
      isOwner: isOwner,
      statusOfMeeting: statusOfMeeting,
      finalPlaceName: finalPlaceName,
    });
  };

  return (
    <TouchableOpacity
      onPress={viewMeeting}
      style={{
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
      }}>
      <Text style={styles.primary}>{title}</Text>
      <Text>
        <Icon name="calendar" size={20} /> &nbsp;
        {time}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  primary: {
    fontSize: 18,
    fontWeight: '700',
  },
});

export default MeetingItem;
