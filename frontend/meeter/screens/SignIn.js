import React, {useState} from 'react';
import {View, Text, StyleSheet, TextInput, Alert} from 'react-native';
import SuccessButton from '../components/SuccessButton';
import globalStyles from '../styles/globalStyles';
import PrimaryButton from '../components/PrimaryButton';
import {apiPost, genSignInForm} from '../api/apiServices';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';

const SignIn = ({navigation}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passVisibility, setPassVisibility] = useState(false);
  const [errorAlert, setErrorAlert] = useState('');

  const signIn = () => {
    apiPost(genSignInForm(username, password)).then(res => {
      if (res.status == 'success') {
        Alert.alert('Zalogowano');

        setUsername('');
        setPassword('');
        setErrorAlert('');

        navigation.navigate('Home', {
          sessionId: res.sessionID,
          username: res.username,
        });
      } else {
        setErrorAlert('' + res['errorMessage']);
      }
    });
  };

  return (
    <View style={styles.centerView}>
      <Text style={globalStyles.title}>Zaloguj się</Text>

      <Text style={globalStyles.label}>Username lub email</Text>
      <TextInput
        style={globalStyles.inputField}
        editable
        maxLength={40}
        value={username}
        onChangeText={text => setUsername(text)}
      />

      <View style={{flexDirection: 'row'}}>
        <Text style={globalStyles.label}>Hasło</Text>
        <TouchableOpacity onPress={() => setPassVisibility(!passVisibility)}>
          <Icon name="eye" size={20} />
        </TouchableOpacity>
      </View>
      <View style={{flex: 1}}>
        <TextInput
          secureTextEntry={!passVisibility}
          style={globalStyles.inputField}
          editable
          maxLength={40}
          value={password}
          onChangeText={text => setPassword(text)}
        />
      </View>

      <Text style={globalStyles.inputAlert}>{errorAlert}</Text>

      <SuccessButton
        active={password.length && username.length}
        onPress={signIn}
        text="Zaloguj"
      />
      <PrimaryButton
        text="Zarejestruj się"
        onPress={() => navigation.navigate('SignUp')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  centerView: {
    flex: 1,
    justifyContent: 'center',
  },
});

export default SignIn;
