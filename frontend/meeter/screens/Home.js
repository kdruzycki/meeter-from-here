import React from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import {apiPost, genSignOutForm} from '../api/apiServices';
import PrimaryButton from '../components/PrimaryButton';

const Home = ({route, navigation}) => {
  const {sessionId, username} = route.params;

  const signOut = () => {
    // wyślij session do backend, aby sie wylogowac
    apiPost(genSignOutForm(sessionId)).then(res => {
      if (res['status'] == 'success') {
        Alert.alert('Wylogowano');

        // TODO: Przejdz do ekranu logowaania
        navigation.navigate('SignIn', {});
      } else {
        setErrorAlert('' + res['errorMessage']);
      }
    });
  };
  return (
    <View style={styles.view}>
      <Text style={styles.logo}>Meeter</Text>
      <PrimaryButton
        text="Zorganizuj spotkanie"
        onPress={() => navigation.navigate('InitMeeting', route.params)}
      />
      <PrimaryButton
        text="Moje spotkania"
        onPress={() => navigation.navigate('MyMeetings', route.params)}
      />
      <PrimaryButton
        text="Dołącz do spotkania"
        onPress={() => navigation.navigate('Join', route.params)}
      />
      <PrimaryButton
        onPress={() => navigation.navigate('Settings', route.params)}
        text="Ustawienia"
      />
      <PrimaryButton onPress={signOut} text="Wyloguj" />
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    fontFamily: 'Pacifico-Regular',
    color: '#f39c12',
    fontSize: 40,
    textAlign: 'center',
  },
});

export default Home;
