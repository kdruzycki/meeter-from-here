import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import globalStyles from '../styles/globalStyles';
import PrimaryButton from '../components/PrimaryButton';
import {FlatList} from 'react-native-gesture-handler';
import MeetingItem from '../components/MeetingItem';
import {apiPost, genDownloadMeetingsForm} from '../api/apiServices';

const MyMeetings = ({route, navigation}) => {
  const {sessionId, username} = route.params;
  const [meetings, setMeetings] = useState([]);
  const [meetingsLoaded, setMeetingsLoaded] = useState(false);
  const [errorAlert, setErrorAlert] = useState('');

  let parseDate = time => {
    let a = time.substr(0, 10) + 'T' + time.substr(11, 8);
    return a;
  };
  if (!meetingsLoaded) {
    (async () => {
      await apiPost(genDownloadMeetingsForm(sessionId))
        .then(res => {
          if (res['status'] == 'success') setMeetings(res['meetings']);
          else setErrorAlert('' + res['errorMessage']);
        })
        .catch(err => setErrorAlert('' + err));
      setMeetingsLoaded(true);
    })();
  }

  return (
    <View style={styles.centerView}>
      <Text style={globalStyles.title}>Moje spotkania</Text>

      <FlatList
        data={meetings}
        renderItem={({item}) => (
          <View
            style={
              new Date(Date.parse(parseDate(item.time))) < new Date()
                ? styles.futureMeetingCard
                : styles.pastMeetingCard
            }>
            <MeetingItem
              key={item.meetingID}
              title={item.name}
              time={item.time}
              meetingId={item.meetingID}
              sessionId={sessionId}
              navigation={navigation}
              username={username}
              willAttend={item.willAttend}
              isOwner={item.isOwner}
              statusOfMeeting={item.decision}
              finalPlaceName={
                item.finalPlace !== undefined ? item.finalPlace.name : ''
              }
            />
          </View>
        )}
        keyExtractor={(item, index) => item.meetingID}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  centerView: {
    flex: 1,
    justifyContent: 'center',
  },
  futureMeetingCard: {
    backgroundColor: '#fff',
    marginVertical: 10,
    marginHorizontal: 20,
    padding: 20,
    flexDirection: 'row',
  },
  pastMeetingCard: {
    backgroundColor: 'rgba(52, 52, 52, 0.1)',
    marginVertical: 10,
    marginHorizontal: 20,
    padding: 10,
    flexDirection: 'row',
  },
});

export default MyMeetings;
