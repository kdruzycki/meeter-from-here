import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  TextInput,
  ScrollView,
  Alert,
  Clipboard,
} from 'react-native';
import SuccessButton from '../components/SuccessButton';
import PrimaryButton from '../components/PrimaryButton';
import globalStyles from '../styles/globalStyles';

import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Entypo';
import {
  apiPost,
  genGeneratePlacesForm,
  genUserStatusForm,
  genDownloadMembersForm,
} from '../api/apiServices';
import email from 'react-native-email';
import ReactInterval from 'react-interval';

const Meeting = ({route, navigation}) => {
  const {
    sessionId,
    meetingId,
    username,
    willAttend,
    title,
    isOwner,
    statusOfMeeting,
    finalPlaceName,
  } = route.params;

  const [errorAlert, setErrorAlert] = useState('');
  const [members, setMembers] = useState([]);
  const [membersLoaded, setMembersLoaded] = useState(false);
  const [mail, setMail] = useState('');
  const [status, setStatus] = useState(willAttend);

  const handleEmail = () => {
    email(mail, {
      subject: `${username} invided you to the meeting!`,
      body: `Join the meeting. The meeting id is: ${meetingId}`,
    }).catch(console.error);
  };

  const toogleCheck = async () => {
    let s = status ? 'false' : 'true';
    await apiPost(genUserStatusForm(meetingId, sessionId, s))
      .then(res => {
        if ((res.status = 'success')) setStatus(!status);
      })
      .catch(err => console.log(err));
  };

  const downloadMembers = async () => {
    await apiPost(genDownloadMembersForm(sessionId, meetingId))
      .then(res => {
        if (res.status == 'success') setMembers(res.users);
        else setErrorAlert('' + res.errorMessage);
      })
      .catch(err => setErrorAlert('' + err));

    setMembersLoaded(true);
  };

  if (!membersLoaded) {
    (async () => downloadMembers())();
  }

  const goToVoting = () => {
    navigation.navigate('Voting', {
      sessionId: sessionId,
      meetingId: meetingId,
      title: title,
      willAttend: willAttend,
      username: username,
      isOwner: isOwner,
      statusOfMeeting: statusOfMeeting,
      finalPlaceName: finalPlaceName,
    });
  };

  const goToDecision = () => {
    navigation.navigate('Decision', {
      sessionId: sessionId,
      meetingId: meetingId,
      title: title,
      willAttend: willAttend,
      username: username,
      isOwner: isOwner,
      statusOfMeeting: statusOfMeeting,
      finalPlaceName: finalPlaceName,
    });
  };

  const genPLaces = async () => {
    await apiPost(genGeneratePlacesForm(sessionId, meetingId))
      .then(res => {
        console.log(res);
        if (res['status'] == 'success') alert('Wygenerowano miejsca');
        else alert('' + res['errorMessage']);
      })
      .catch(err => console.log(err));
  };

  return (
    <View style={styles.centerView}>
      <View style={{flexDirection: 'row-reverse', margin: 10}}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Map', {
              sessionId: sessionId,
              meetingId: meetingId,
              members: members,
            });
          }}>
          <Icon name="map" size={40} />
        </TouchableOpacity>
      </View>

      <Text
        style={[
          globalStyles.title,

          statusOfMeeting == 'cancelled'
            ? {textDecorationLine: 'line-through'}
            : {},
        ]}>
        {' '}
        {title}
      </Text>

      <TouchableOpacity
        onPress={toogleCheck}
        style={{flexDirection: 'row', justifyContent: 'center'}}>
        <Text style={{fontSize: 20}}>Obecność </Text>
        {status ? (
          <Icon name="check" size={24} />
        ) : (
          <Icon name="cross" size={24} />
        )}
      </TouchableOpacity>

      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.scrollView}>
        <View style={styles.body}>
          {members.map(user => {
            if (user.username.toUpperCase() == username.toUpperCase()) return;
            return (
              <View key={user.username} style={styles.userCard}>
                <Text style={styles.userName}>{user.username}</Text>
                {user.willAttend ? (
                  <Icon style={styles.attendIcon} name="check" size={24} />
                ) : (
                  <Icon name="cross" size={24} />
                )}
              </View>
            );
          })}
        </View>
        <Text style={globalStyles.inputAlert}>{errorAlert}</Text>
      </ScrollView>

      <View>
        {statusOfMeeting == 'placeChosen' ? (
          <Text style={styles.info}>Miejsce wybrane {finalPlaceName}</Text>
        ) : (
          <Text />
        )}
      </View>

      <SuccessButton
        onPress={() => {
          Clipboard.setString(meetingId);
          Alert.alert('Skopiowano ID spotkania.');
        }}
        text="Kopiuj ID"
      />

      {isOwner ? (
        <PrimaryButton onPress={genPLaces} text="Generuj miejsca" />
      ) : (
        <View />
      )}

      {isOwner ? (
        <PrimaryButton onPress={goToDecision} text="Wybierz miejsce" />
      ) : (
        <Text />
      )}

      <PrimaryButton onPress={goToVoting} text="Głosuj" />

      {isOwner ? (
        <View>
          <TextInput
            style={globalStyles.inputField}
            editable
            placeholder="email"
            maxLength={40}
            value={mail}
            onChangeText={text => setMail(text)}
          />

          <SuccessButton onPress={handleEmail} text="Zaproś" />
        </View>
      ) : (
        <View />
      )}

      <ReactInterval
        timeout={5000}
        enabled={true}
        callback={downloadMembers}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  centerView: {
    flex: 1,
    justifyContent: 'center',
  },
  userCard: {
    backgroundColor: '#fff',
    marginVertical: 10,
    marginHorizontal: 20,
    flexDirection: 'row',
  },
  userName: {
    margin: 10,
    fontSize: 22,
    fontWeight: '700',
  },
  attendIcon: {
    marginTop: 14,
  },
  info: {
    marginLeft: 20,
    fontSize: 20,
    fontWeight: '700',
    textAlign: 'center',
  },
  title: {
    fontSize: 18,
    fontWeight: '700',
    textAlign: 'center',
    textAlign: 'center',
  },
});

export default Meeting;
