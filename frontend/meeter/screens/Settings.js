import React, {useState} from 'react';
import {View, Text, StyleSheet, TextInput, Alert} from 'react-native';
import SuccessButton from '../components/SuccessButton';
import globalStyle from '../styles/globalStyles';
import {validName} from '../validation';
import {apiPost, genChangeUsernameForm} from '../api/apiServices';
import globalStyles from '../styles/globalStyles';

const Settings = ({route, navigation}) => {
  const {sessionId, username} = route.params;
  const [name, setName] = useState('');
  const [errorAlert, setErrorAlert] = useState('');

  const changeUsername = () => {
    apiPost(genChangeUsernameForm(name, sessionId)).then(res => {
      if (res['status'] == 'success') {
        Alert.alert('Zmieniono nazwę użytkownika na: ' + name);
        setName('');
        setErrorAlert('');
        navigation.navigate('Home', {
          sessionId: sessionId,
          username: name,
        });
      } else {
        setErrorAlert('' + res['errorMessage']);
      }
    });
  };

  return (
    <View style={styles.centerView}>
      <Text style={globalStyle.title}>Ustawienia</Text>

      <Text style={globalStyle.label}>Zmień nazwę użytkownika</Text>
      <TextInput
        style={globalStyle.inputField}
        editable
        maxLength={32}
        placeholder="nowa nazwa"
        value={name}
        onChangeText={text => setName(text)}
      />

      <Text style={globalStyles.inputAlert}>{errorAlert}</Text>

      <SuccessButton
        active={validName(name).valid}
        onPress={changeUsername}
        text="Zatwierdź"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  centerView: {
    flex: 1,
    justifyContent: 'center',
  },
});

export default Settings;
