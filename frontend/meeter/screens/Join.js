import React, {useState} from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import SuccessButton from '../components/SuccessButton';
import globalStyle from '../styles/globalStyles';
import GetLocation from 'react-native-get-location';
import globalStyles from '../styles/globalStyles';
import {apiPost, genUserJoinedForm} from '../api/apiServices';

const Join = ({route, navigation}) => {
  const {sessionId, username} = route.params;
  const [meetingId, setMeetingId] = useState('');
  const [errorAlert, setErrorAlert] = useState('');

  const getLocation = async () => {
    return await GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    }).catch(error => console.log(error));
  };

  const join = async () => {
    let location = await getLocation();
console.log(meetingId)
    apiPost(genUserJoinedForm(sessionId, meetingId, location))
      .then(res => {
        if (res['status'] == 'success') {
          navigation.navigate('MyMeetings', {
            sessionId: sessionId,
            meetingId: meetingId,
            username: username,
          });
        } else {
          setErrorAlert('' + res['errorMessage']);
        }
      })
      .catch(err => setErrorAlert('' + err));
  };

  return (
    <View style={styles.centerView}>
      <Text style={globalStyle.title}>Dołącz do spotkania</Text>

      <Text style={globalStyle.label}>Id spotkania</Text>
      <TextInput
        style={globalStyle.inputField}
        editable
        maxLength={40}
        value={meetingId}
        onChangeText={text => setMeetingId(text)}
      />

      <Text style={globalStyles.inputAlert}>{errorAlert}</Text>

      <SuccessButton
        /* TODO: Sprawdzanie formularza */
        /* TODO: Wysyłanie formularza */
        onPress={() => join()}
        text="Dołącz"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  centerView: {
    flex: 1,
    justifyContent: 'center',
  },
});

export default Join;
