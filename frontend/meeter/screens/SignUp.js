import React, {useState} from 'react';
import {View, Text, StyleSheet, TextInput, Alert} from 'react-native';
import SuccessButton from '../components/SuccessButton';
import globalStyles from '../styles/globalStyles';
import {apiPost, genSignInForm, genSignUpForm} from '../api/apiServices';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';

const SignUp = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passVisibility, setPassVisibility] = useState(false);
  const [errorAlert, setErrorAlert] = useState('');

  const signUp = () => {
    // TODO: Przenisc to do jakiegos osobnego pliku

    //TODO: Sprawdzić czy wgl można tak hasło trzymac w state
    // TODO:Uprzątnoć to:
    apiPost(genSignUpForm(username, email, password)).then(res => {
      if (res['status'] == 'success') {
        Alert.alert('Rejestracja przeszła pomyślnie');
        setPassword('');
        setUsername('');
        setEmail('');

        apiPost(genSignInForm(username, password)).then(res => {
          if (res['status'] == 'success') {
            Alert.alert('Zarejestrowano');
            navigation.navigate('Home', {
              sessionId: res.sessionID,
              username: username,
            });
          } else {
            navigation.navigate('SignIn');
          }
        });
      } else {
        setErrorAlert('' + res['errorMessage']);
      }
    });
  };

  return (
    <View style={styles.centerView}>
      <Text style={globalStyles.title}>Zarejestuj się</Text>

      <Text style={globalStyles.label}>Email</Text>
      <TextInput
        style={globalStyles.inputField}
        editable
        maxLength={40}
        value={email}
        onChangeText={text => setEmail(text)}
      />

      <Text style={globalStyles.label}>Username</Text>
      <TextInput
        style={globalStyles.inputField}
        editable
        maxLength={40}
        value={username}
        onChangeText={text => setUsername(text)}
      />

      <View style={{flexDirection: 'row'}}>
        <Text style={globalStyles.label}>Hasło</Text>
        <TouchableOpacity onPress={() => setPassVisibility(!passVisibility)}>
          <Icon name="eye" size={20} />
        </TouchableOpacity>
      </View>
      <TextInput
        secureTextEntry={!passVisibility}
        style={globalStyles.inputField}
        editable
        maxLength={40}
        value={password}
        onChangeText={text => setPassword(text)}
      />

      <Text style={globalStyles.inputAlert}>{errorAlert}</Text>

      <SuccessButton
        active={email.length && password.length && username.length}
        onPress={signUp}
        text="Dołącz"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  centerView: {
    flex: 1,
    justifyContent: 'center',
  },
});

export default SignUp;
