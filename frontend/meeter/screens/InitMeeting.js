import React, {useState} from 'react';
import {View, Text, Platform, StyleSheet, Alert, TextInput} from 'react-native';
import SuccessButton from '../components/SuccessButton';
import DateTimePicker from '@react-native-community/datetimepicker';
import globalStyle from '../styles/globalStyles';
import globalStyles from '../styles/globalStyles';
import {validDate, validName} from '../validation';
import GetLocation from 'react-native-get-location';
import {apiPost, genCreateMeetingForm} from '../api/apiServices';

const InitMeeting = ({route, navigation}) => {
  const {sessionId, username} = route.params;

  const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  const [name, setName] = useState('');
  const [errorAlert, setErrorAlert] = useState('');

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };

  const formatDate = date => {
    const d = date.getDate();
    const m = date.getMonth() + 1;
    const y = date.getFullYear();

    return `${d}.${m < 10 ? '0' + m : m}.${y}`;
  };

  const getTime = date => {
    const h = date.getHours();
    const m = date.getMinutes();

    return `${h}:${m < 10 ? '0' + m : m}`;
  };

  const createMeeting = async () => {
    let location = await GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    }).catch(error => {
      const {code, message} = error;
      setErrorAlert('' + message);
    });
    const dateFormatted = ""+date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()+':00';
    console.log(dateFormatted);
    await apiPost(genCreateMeetingForm(name, dateFormatted, location, sessionId))
      .then(res => {
        if (res['status'] == 'success') {
          setName('');
          setDate(new Date());
          console.log(genCreateMeetingForm(name, dateFormatted, location, sessionId));
          Alert.alert('Utworzono spotkanie');

          navigation.navigate('MyMeetings', {
            sessionId: sessionId,
            meetingId: res['meetingID'],
            username: username,
            willAttend: true,
            title: name,
            isOwner: true,
          });
        } else {
          setErrorAlert('' + res['errorMessage']);
        }
      })
      .catch(err => setErrorAlert('' + err));
  };

  return (
    <View>
      <Text style={globalStyle.title}>Zorganizuj spotkanie</Text>

      <TextInput
        placeholder="Nazwa spotkania"
        style={globalStyle.inputField}
        editable
        maxLength={32}
        value={name}
        onChangeText={text => setName(text)}
      />
      {!validName(name).valid && (
        <Text style={globalStyles.inputAlert}>{validName(name).alert}</Text>
      )}

      <Text style={globalStyle.inputField} onPress={showDatepicker}>
        {formatDate(date)}
      </Text>
      <Text style={globalStyle.inputField} onPress={showTimepicker}>
        {getTime(date)}
      </Text>

      {!validDate(date).valid && (
        <Text style={globalStyles.inputAlert}>{validDate(date).alert}</Text>
      )}

      {show && (
        <DateTimePicker
          testID="dateTimePicker"
          timeZoneOffsetInMinutes={0}
          value={date}
          mode={mode}
          is24Hour={true}
          display="default"
          onChange={onChange}
        />
      )}

      <Text style={globalStyles.inputAlert}>{errorAlert}</Text>

      <SuccessButton
        active={validDate(date).valid && validName(name).valid}
        pointerEvents="none"
        onPress={createMeeting}
        text="Potwierdź"
      />
    </View>
  );
};

export default InitMeeting;
