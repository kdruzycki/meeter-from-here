import React, {useState, useCallback} from 'react';
import {View, Text, StyleSheet, ScrollView, Alert} from 'react-native';
import SuccessButton from '../components/SuccessButton';
import {TouchableOpacity} from 'react-native-gesture-handler';
import globalStyles from '../styles/globalStyles';
import {
  apiPost,
  genDownloadPlacesForm,
  genVoteForPlacesForm,
} from '../api/apiServices';
import ReactInterval from 'react-interval';

const Voting = ({route, navigation}) => {
  const {
    sessionId,
    meetingId,
    username,
    willAttend,
    title,
    statusOfMeeting,
  } = route.params;

  const [errorAlert, setErrorAlert] = useState('');
  const [places, setPlaces] = useState([]);
  const [placesLoaded, setPlacesLoaded] = useState(false);
  const [chosen, setChosen] = useState([]);

  const [unusedState, setUnusedState] = useState();
  const forceUpdate = useCallback(() => setUnusedState({}), []);

  const downloadPlaces = async () => {
    await apiPost(genDownloadPlacesForm(sessionId, meetingId))
      .then(res => {
        if (res.status == 'success') setPlaces(res['places']);
        else setErrorAlert('' + res.errorMessage);
      })
      .catch(err => setErrorAlert('' + err));

    setPlacesLoaded(true);
  };
  if (!placesLoaded) {
    (async () => downloadPlaces())();
    let placeIDs = chosen;

    for (let i = 0; i < places.length; i++) {
      console.log('place' + JSON.stringify(places[i]));
      for (let j = 0; j < places[i].voters.length; j++) {
        if (places[i].voters[j].username == username) {
          placeIDs.push(places[i].placeID);
          console.log(places[i].placeID);
          // setChosen(placeIDs);
        }
      }
    }
    console.log(places);
  }

  const addVote = placeID => {
    let placeIDs = chosen;
    if (placeIDs.indexOf(placeID) == -1) {
      placeIDs.push(placeID);
      // Alert.alert('dodano miejsce');
    } else {
      placeIDs.splice(placeIDs.indexOf(placeID), 1);
      // Alert.alert('usunieto miejsce');
    }
    setChosen(placeIDs);
    forceUpdate();
  };

  const vote = () => {
    apiPost(genVoteForPlacesForm(sessionId, meetingId, chosen))
      .then(res => {
        if (res['status'] == 'success') console.log('success');
        else setErrorAlert('' + res['errorMessage']);
        navigation.navigate('Meeting', {
          sessionId: sessionId,
          meetingId: meetingId,
          title: title,
          willAttend: willAttend,
          username: username,
          statusOfMeeting: statusOfMeeting,
        });
      })
      .catch(err => setErrorAlert('' + err));
  };

  return (
    <View style={styles.centerView}>
      <Text style={globalStyles.title}>{title}</Text>
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.scrollView}>
        <View style={styles.body}>
          {places.map(place => {
            return (
              <TouchableOpacity
                key={place.placeID}
                onPress={() => {
                  console.log('pres');
                  addVote(place.placeID);
                }}>
                <View
                  style={
                    chosen.indexOf(place.placeID) != -1
                      ? styles.chosenCard
                      : styles.nonChosenCard
                  }>
                  <Text style={styles.placeName}>{place.name}</Text>
                  <Text style={styles.vote}>{place.voters.length} głosy</Text>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      </ScrollView>
      <SuccessButton
        onPress={() => {
          vote();
          Alert.alert('Wysłano głosowanie');
        }}
        text="Wyślij głosy"
      />
      <ReactInterval timeout={5000} enabled={true} callback={downloadPlaces} />
    </View>
  );
};
const styles = StyleSheet.create({
  centerView: {
    flex: 1,
    justifyContent: 'center',
  },
  centerView: {
    flex: 1,
    justifyContent: 'center',
  },
  nonChosenCard: {
    backgroundColor: '#fff',
    marginVertical: 10,
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  chosenCard: {
    backgroundColor: '#f39c12',
    marginVertical: 10,
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  placeName: {
    margin: 10,
    fontSize: 22,
    fontWeight: '700',
    maxWidth:'75%'
  },
  attendIcon: {
    marginTop: 14,
  },
  vote:{
    position:'absolute',
    right:0,
    fontSize: 22,
    fontWeight: '700',
    margin: 10,
  }
});

export default Voting;
