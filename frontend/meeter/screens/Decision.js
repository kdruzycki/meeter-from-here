import React, {useState, useCallback} from 'react';
import {View, Text, StyleSheet, ScrollView, Alert} from 'react-native';
import SuccessButton from '../components/SuccessButton';
import PrimaryButton from '../components/PrimaryButton';
import globalStyles from '../styles/globalStyles';
import {
  apiPost,
  genDownloadPlacesForm,
  genSetDecisionForm,
} from '../api/apiServices';
import {TouchableOpacity} from 'react-native-gesture-handler';

const Decision = ({route, navigation}) => {
  const {
    sessionId,
    meetingId,
    username,
    willAttend,
    title,
    isOwner,
    statusOfMeeting,
    finalPlaceName,
  } = route.params;

  const [errorAlert, setErrorAlert] = useState('');
  const [places, setPlaces] = useState([]);
  const [placesLoaded, setPlacesLoaded] = useState(false);
  const [placeId, setPlaceId] = useState(-1);
  const [finalPlace, setFinalPlace] = useState('');
  // const [statusSent, setStatusSent] = useState('placeUnchosen');

  const [unusedState, setUnusedState] = useState();
  const forceUpdate = useCallback(() => setUnusedState({}), []);

  let statusSent = 'placeUnchosen';

  if (!placesLoaded) {
    (async () => {
      await apiPost(genDownloadPlacesForm(sessionId, meetingId))
        .then(res => {
          if (res['status'] == 'success') setPlaces(res['places']);
          else setErrorAlert('' + res['errorMessage']);
        })
        .catch(err => setErrorAlert('' + err));
      setPlacesLoaded(true);
    })();
  }
  const choose = (placeID, name) => {
    setPlaceId(placeID);
    setFinalPlace(name);

    forceUpdate();
  };
  // "status": "placeChosen", 	//dopuszczalne wartości to "placeChosen", "cancelled", "placeUnchosen"
  const choosePlace = () => {
    let placeIDset = placeId;

    if (placeIDset == -1) {
      statusSent = 'placeUnchosen';
    } else if (placeIDset == -2) {
      statusSent = 'cancelled';
    } else {
      statusSent = 'placeChosen';
    }
    if (finalPlace == '') {
      setFinalPlace(finalPlaceName);
    }

    console.log(statusSent);

    apiPost(genSetDecisionForm(sessionId, meetingId, statusSent, placeIDset))
      .then(res => {
        if (res['status'] == 'success') console.log('success');
        else {
          setErrorAlert('' + res['errorMessage']);
          console.log(
            genSetDecisionForm(sessionId, meetingId, statusSent, placeIDset),
          );

          statusSent = statusOfMeeting;
          console.log('reserror');
          console.log(res['errorMessage']);
        }
        console.log('res');
        navigation.navigate('Meeting', {
          sessionId: sessionId,
          meetingId: meetingId,
          title: title,
          willAttend: willAttend,
          username: username,
          isOwner: isOwner,
          statusOfMeeting: statusSent,
          finalPlaceName: finalPlace,
        });
      })
      .catch(err => {
        setErrorAlert('' + err);
        console.log(
          genSetDecisionForm(sessionId, meetingId, statusSent, placeIDset),
        );
        console.log(err);
      });
  };
  return (
    <View style={styles.centerView}>
      <Text style={globalStyles.title}>{title}</Text>
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.scrollView}>
        <View style={styles.body}>
          {places.map(place => {
            return (
              <TouchableOpacity
                key={place.placeID}
                onPress={() => {
                  choose(place.placeID, place.name);
                }}>
                <View
                  style={
                    placeId == place.placeID
                      ? styles.chosenCard
                      : styles.nonChosenCard
                  }>
                  <Text style={styles.placeName}>{place['name']}</Text>
                  <Text style={styles.vote}>{place['voters'].length} głosy</Text>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>

        <TouchableOpacity
          key={-2}
          onPress={() => {
            choose(-2, '');
          }}>
          <View
            style={placeId == -2 ? styles.chosenCard : styles.nonChosenCard}>
            <Text style={styles.placeName}>Anuluj spotkanie</Text>
          </View>
        </TouchableOpacity>
      </ScrollView>
      <SuccessButton
        onPress={() => {
          choosePlace();
          Alert.alert('Wysłano wybór miejsca');
        }}
        text="Wyślij wybór"
      />
    </View>
  );
};
const styles = StyleSheet.create({
  centerView: {
    flex: 1,
    justifyContent: 'center',
  },
  userCard: {
    height: 100,
    backgroundColor: '#fff',
    marginVertical: 10,
    marginHorizontal: 20,
    justifyContent: 'center',
  },
  placeName: {
    margin: 10,
    fontSize: 22,
    fontWeight: '700',
    maxWidth:'75%'

  },
  chosen: {
    height: 100,
    width: 200,
    marginVertical: 10,
    marginHorizontal: 20,
    justifyContent: 'center',
    backgroundColor: '#ff0',
    textAlign: 'center',
    paddingHorizontal: 28,
    paddingVertical: 14,
    borderRadius: 20,
    margin: 10,
    alignSelf: 'center',
  },
  nonChosenCard: {
    backgroundColor: '#fff',
    marginVertical: 10,
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  chosenCard: {
    backgroundColor: '#f39c12',
    marginVertical: 10,
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  vote:{
    position:'absolute',
    right:0,
    fontSize: 22,
    fontWeight: '700',
    margin: 10,
  }
});

export default Decision;
