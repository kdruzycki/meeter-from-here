import React, {useState} from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import {
  apiPost,
  genGeneratePlacesForm,
  genDownloadPlacesForm,
} from '../api/apiServices';
import {Marker} from 'react-native-maps';

const Map = ({route, navigation}) => {
  const {sessionId, meetingId, members} = route.params;

  const [places, setPlaces] = useState([]);
  const [placesLoaded, setPlacesLoaded] = useState(false);

  if (!placesLoaded) {
    (async () => {
      await apiPost(genDownloadPlacesForm(sessionId, meetingId))
        .then(res => {
          if (res['status'] == 'success') setPlaces(res['places']);
          else console.log('' + res['errorMessage']);
        })
        .catch(err => console.log(err));
      setPlacesLoaded(true);
    })();
  }

  const markPlaces = () => {
    if (!placesLoaded) return;
    return places.map(place => (
      <Marker
        coordinate={{
          latitude: parseFloat(place.latitude),
          longitude: parseFloat(place.longitude),
        }}
        title={place.name}
        pinColor="orange"
        key={place.placeID}
      />
    ));
  };

  const markMembers = () => {
    if (!members) return;
    return members.map(user => (
      <Marker
        coordinate={{
          latitude: parseFloat(user.latitude),
          longitude: parseFloat(user.longitude),
        }}
        title={user.username}
        pinColor="green"
        key={user.username}
      />
    ));
  };

  return (
    <View style={styles.container}>
      {placesLoaded ? (
        <MapView
          provider={PROVIDER_GOOGLE} // remove if not using Google Maps
          style={styles.map}
          region={{
            latitude: parseFloat(members[0].latitude),
            longitude: parseFloat(members[0].longitude),
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}>
          {markPlaces()}
          {markMembers()}
        </MapView>
      ) : (
        <Text>Loading</Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default Map;
