# Uruchamianie aplikacji react native

Tutaj wszystko jest opisane:
https://reactnative.dev/docs/environment-setup

Ogólnie:
Potrzebny jest node, android sdk, ewentualnie android VD (AVD)

Gdy będziecie mieli już wszystko zainstalowane i utworzony avd (można też podłączyć telefon):

1.W folderze aplikacji:
npx react-native start

2.W innym oknie terminala:
npx react-native run-android

Powinien się uruchomić AVD z aplikacją.
